# Working Software Conf 2021

[![pipeline status](https://gitlab.com/italian-agile-movement/working-software/badges/master/pipeline.svg)](https://gitlab.com/italian-agile-movement/working-software/commits/master)


Working Software Conference website repo
[Working Software](https://www.agilemovement.it/workingsoftware)


--- 
## Run locally
You can use _NodeJS_ to run the website locally.

Linux:
~~~ sh
$ cd workingsoftware  
$ npm install
$ node node_modules/.bin/static -p 8081
~~~ 

Windows (from command prompt):
~~~ dos
cd workingsoftware 
node node_modules\node-static\bin\cli.js -p 8081
~~~ 
