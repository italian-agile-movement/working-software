# Working Software Conference

[![pipeline status](https://gitlab.com/italian-agile-movement/working-software/badges/main/pipeline.svg)](https://gitlab.com/italian-agile-movement/working-software/commits/main)


[Working Software Conference](https://www.agilemovement.it/workingsoftware)


--- 
## Run locally
You can use _Node.js_ to run the website locally.

~~~ sh
$ npm install
$ npm start
~~~

## Mobile menu

In order to handle mobile menu button, we took scripts and styles from another template

Template Name: TheEvent
Template URL: https://bootstrapmade.com/theevent-conference-event-bootstrap-template/
Author: BootstrapMade.com
License: https://bootstrapmade.com/license/

We copied the `asset` folder from the template and added references inside `index.html`
Then we cherry picked the desired css from the template to handle the mobile menu and put it inside `app.css`

